import React from 'react';
import {ApolloClient, InMemoryCache, ApolloProvider} from '@apollo/client';
import HomeScreen from './HomeScreen';

const client = new ApolloClient({
  cache: new InMemoryCache(),
  uri: 'http://localhost:4000',
});

const App = () => (
  <ApolloProvider client={client}>
    <HomeScreen />
  </ApolloProvider>
);

export default App;
